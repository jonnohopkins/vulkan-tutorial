![application render](vk_msaa.png)

## About

Main differences with the tutorial's source code:

- uses CMake for build depedency management.
- uses the Vulkan Hpp C++ bindings
- inline many functions if they are only called once
- runtime compiling of shaders using shaderc
- pause rendering when app is not in focus

## Dependencies

Manual:

 - [CMake](https://cmake.org/download/)
 - [Vulkan SDK](https://www.lunarg.com/vulkan-sdk/)

Managed by CMake:

 - [GLFW](https://github.com/glfw/glfw)
 - [GLM](https://github.com/g-truc/glm)

Included in source:

 - [stb_image](https://github.com/nothings/stb)
 - [tinyobjloader](https://github.com/tinyobjloader/tinyobjloader)

## Building

Select a `<dir>` to generate the project files and run:
 ```
cmake -B <dir>			# create the project
cmake --build <dir>		# build the project
cmake --open <dir>		# open the project
```
