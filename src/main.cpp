#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/hash.hpp>
#include <vulkan/vulkan.hpp>
#include <shaderc/shaderc.hpp>
#include <stb_image.h>
#include <tiny_obj_loader.h>
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <cstdlib>
#include <cstring>
#include <vector>
#include <set>
#include <algorithm>
#include <array>
#include <chrono>
#include <unordered_map>

#ifdef WIN32
#define BREAKPOINT __debugbreak()
#else
#define BREAKPOINT
#endif

#define ASSERT(condition, explanation) if(!(condition)) { BREAKPOINT; }
#define FATAL_ASSERT(condition, explanation) if(!(condition)) { BREAKPOINT; abort(); }

namespace xtd {
    // contains(container, value)
    // returns true iff one of the elements is equal to value.
    template<typename Container>
    bool contains(const Container& container, typename Container::value_type&& value) {
        return std::find(container.begin(), container.end(), std::forward<typename Container::value_type>(value)) != container.end();
    }

    template<typename Container>
    bool contains(const Container& container, const typename Container::value_type& value) {
        return std::find(container.begin(), container.end(), value) != container.end();
    }
}

const char* to_string(shaderc_compilation_status status) {
    switch (status) {
    case shaderc_compilation_status_success:
        return "success";
    case shaderc_compilation_status_invalid_stage:
        return "invalid_stage";
    case shaderc_compilation_status_compilation_error:
        return "compilation_error";
    case shaderc_compilation_status_internal_error:
        return "internal_error";
    case shaderc_compilation_status_null_result_object:
        return "null_result_object";
    case shaderc_compilation_status_invalid_assembly:
        return "invalid_assembly";
    case shaderc_compilation_status_validation_error:
        return "validation_error";
    case shaderc_compilation_status_transformation_error:
        return "transformation_error";
    case shaderc_compilation_status_configuration_error:
        return "configuration_error";
    default:
        throw std::logic_error{ "unrecognised value" };
    }
}

std::string load_shader_from_file(const char* path) {
    std::string shader_source;

    std::ifstream shader_file;
    shader_file.exceptions(std::ifstream::failbit | std::ifstream::badbit);

    try {
        shader_file.open(path, std::ios::ate | std::ios::binary);
        size_t file_size = shader_file.tellg();
        shader_source.resize(file_size);
        shader_file.seekg(0);
        shader_file.read(shader_source.data(), file_size);
        shader_file.close();
    }
    catch (std::ifstream::failure& e) {
        throw std::runtime_error{ std::string("unable to read shader file: ") + path };
    }

    return shader_source;
}

shaderc::SpvCompilationResult compile_glsl_to_spv(const std::string& shader_source, shaderc_shader_kind shader_kind, const char* input_file_name) {
    shaderc::Compiler compiler{};
    shaderc::CompileOptions compile_options{};
    //compile_options.SetGenerateDebugInfo();
    //compile_options.SetSourceLanguage(shaderc_source_language_glsl);
    compile_options.SetOptimizationLevel(shaderc_optimization_level_performance);
    //compile_options.SetTargetEnvironment(shaderc_target_env_vulkan, 0);
    //compile_options.SetTargetSpirv(shaderc_spirv_version_1_6);
    auto spv = compiler.CompileGlslToSpv(shader_source, shader_kind, input_file_name, compile_options);
    if (spv.GetCompilationStatus() != shaderc_compilation_status_success) {
        std::cerr << "failed: " << to_string(spv.GetCompilationStatus()) << " - errors: " << spv.GetNumErrors() << " warnings: " << spv.GetNumWarnings() << std::endl;
        std::cerr << spv.GetErrorMessage() << std::endl;
        throw std::runtime_error{ "unable to compile shader." };
    }

    return spv;
}

#define VK_ASSERT(expr, assert_msg)             \
    switch(expr) {                              \
    case vk::Result::eSuccess:                  \
    break;                                      \
    default:                                    \
        throw std::runtime_error{ assert_msg }; \
    }

PFN_vkCreateDebugUtilsMessengerEXT  pfnVkCreateDebugUtilsMessengerEXT;
PFN_vkDestroyDebugUtilsMessengerEXT pfnVkDestroyDebugUtilsMessengerEXT;

VKAPI_ATTR VkResult VKAPI_CALL vkCreateDebugUtilsMessengerEXT(VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDebugUtilsMessengerEXT* pMessenger) {
    return pfnVkCreateDebugUtilsMessengerEXT(instance, pCreateInfo, pAllocator, pMessenger);
}

VKAPI_ATTR void VKAPI_CALL vkDestroyDebugUtilsMessengerEXT(VkInstance instance, VkDebugUtilsMessengerEXT messenger, VkAllocationCallbacks const* pAllocator) {
    return pfnVkDestroyDebugUtilsMessengerEXT(instance, messenger, pAllocator);
}

struct Vertex {
    glm::vec3 pos;
    glm::vec3 color;
    glm::vec2 texCoord;

    static const vk::VertexInputBindingDescription BindingDescription;
    static const std::array<vk::VertexInputAttributeDescription, 3> AttributeDescriptions;

    friend bool operator==(const Vertex& lhs, const Vertex& rhs) {
        return lhs.pos == rhs.pos && lhs.color == rhs.color && lhs.texCoord == rhs.texCoord;
    }
};

const vk::VertexInputBindingDescription Vertex::BindingDescription{ 0, sizeof(Vertex), vk::VertexInputRate::eVertex };

const std::array<vk::VertexInputAttributeDescription, 3> Vertex::AttributeDescriptions{
    vk::VertexInputAttributeDescription{ 0, 0, vk::Format::eR32G32B32Sfloat, offsetof(Vertex, pos) },
    vk::VertexInputAttributeDescription{ 1, 0, vk::Format::eR32G32B32Sfloat, offsetof(Vertex, color) },
    vk::VertexInputAttributeDescription{ 2, 0, vk::Format::eR32G32Sfloat, offsetof(Vertex, texCoord) }
};

namespace std {
    template<> struct hash<Vertex> {
        size_t operator()(Vertex const& vertex) const {
            return ((hash<glm::vec3>()(vertex.pos) ^
                (hash<glm::vec3>()(vertex.color) << 1)) >> 1) ^
                (hash<glm::vec2>()(vertex.texCoord) << 1);
        }
    };
}

class Application {
public:
    void run() {
        start_time = std::chrono::high_resolution_clock::now();
        loadModel();
        initWindow();
        initVulkan();
        mainLoop();
        cleanup();
    }

private:

    std::vector<Vertex> vertices;
    std::vector<uint32_t> indices;

    struct UniformBufferObject {
        alignas(16) glm::mat4 model;
        alignas(16) glm::mat4 view;
        alignas(16) glm::mat4 proj;
    };

    struct DeviceInfo {
        uint32_t graphics_family_idx;
        uint32_t present_family_idx;
        vk::SurfaceFormatKHR format;
        vk::PresentModeKHR present_mode;
    };

    struct FrameData {
        vk::CommandBuffer command_buffer;
        vk::Semaphore image_available_sem;
        vk::Semaphore render_finished_sem;
        vk::Fence in_flight_fence;
        vk::Buffer uniform_buffer;
        vk::DeviceMemory uniform_buffer_memory;
    };

    struct LifecycleState {
        // true does not imply that the app has exclusive input (ie. other apps may receive some input events).
        // false does not imply that the app won't receive some input events.
        bool has_input_focus{ false };
        // must not render while false
        bool has_graphics{ false };
    };

    static constexpr size_t max_frames_in_flight = 2;
    static constexpr char* model_path = "viking_room.obj";
    static constexpr char* texture_path = "viking_room.png";

    std::chrono::time_point<std::chrono::high_resolution_clock> start_time;
    GLFWwindow* window;
    vk::Instance instance;
    vk::SurfaceKHR surface;
    vk::DebugUtilsMessengerEXT debug_messenger;
    vk::PhysicalDevice physical_device;
    vk::Device device;
    vk::Queue graphics_queue;
    vk::Queue presentation_queue;
    DeviceInfo device_info;
    vk::SwapchainKHR swapchain;
    std::vector<vk::Image> swapchain_images; // per swapchain
    std::vector<vk::ImageView> swapchain_image_views; // per swapchain
    vk::Format swapchain_image_format;
    vk::Extent2D swapchain_extent;
    vk::RenderPass render_pass;
    vk::PipelineLayout graphics_pipeline_layout;
    vk::Pipeline graphics_pipeline;
    std::vector<vk::Framebuffer> swapchain_framebuffers; // per swapchain
    vk::CommandPool command_pool;
    std::vector<FrameData> frame_data; // size is max_frames_in_flight
    vk::DescriptorPool descriptor_pool;
    vk::DescriptorSetLayout descriptor_set_layout;
    std::vector<vk::DescriptorSet> descriptor_sets; // size is max_frames_in_flight
    vk::Buffer vertex_buffer;
    vk::DeviceMemory vertex_buffer_memory;
    vk::Buffer index_buffer;
    vk::DeviceMemory index_buffer_memory;
    uint32_t mip_levels;
    vk::SampleCountFlagBits msaa_samples;
    vk::Image texture_image;
    vk::DeviceMemory texture_image_memory;
    vk::ImageView texture_image_view;
    vk::Sampler texture_sampler;
    vk::Format depth_format;
    vk::Image color_image;
    vk::DeviceMemory color_image_memory;
    vk::ImageView color_image_view;
    vk::Image depth_image;
    vk::DeviceMemory depth_image_memory;
    vk::ImageView depth_image_view;
    size_t frame_data_idx{ 0 };
    size_t frame_num{ 0 };
    bool is_swapchain_dirty{ false }; // ie. due to a framebuffer resize.
    LifecycleState lifecycle_state;

#ifdef DEBUG
    static constexpr std::array<const char*, 1> validation_layers = {
        "VK_LAYER_KHRONOS_validation"
    };
#else
    static constexpr std::array<const char*, 0> validation_layers = {};
#endif

    static constexpr std::array<const char*, 1> required_device_extensions = {
        VK_KHR_SWAPCHAIN_EXTENSION_NAME
    };

#ifdef DEBUG
    static constexpr bool enable_validation_layers = true;
#else
    static constexpr bool enable_validation_layers = false;
#endif

    void loadModel() {
        tinyobj::attrib_t attrib;
        std::vector<tinyobj::shape_t> shapes;
        std::vector<tinyobj::material_t> materials;
        std::string warn, err;

        if (!tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err, model_path)) {
            std::cerr << "tinyobj loader errors:\n" << err << std::endl;
            throw std::runtime_error{ "unable to load model file." };
        }

        if(!warn.empty())
            std::cerr << "tinyobj loader warnings:\n" << warn << std::endl;

        std::unordered_map<Vertex, uint32_t> vertex_to_index;

        for (const tinyobj::shape_t& shape : shapes) {
            for (const tinyobj::index_t& index : shape.mesh.indices) {
                Vertex vertex{ glm::vec3{ attrib.vertices[3 * index.vertex_index + 0], attrib.vertices[3 * index.vertex_index + 1], attrib.vertices[3 * index.vertex_index + 2] },
                    glm::vec3{ 1.0f, 1.0f, 1.0f },
                    glm::vec2{ attrib.texcoords[2 * index.texcoord_index + 0], 1.0f - attrib.texcoords[2 * index.texcoord_index + 1] } // flip the vertical component
                };

                auto it_find = vertex_to_index.find(vertex);
                if (it_find == vertex_to_index.end()) {
                    it_find = vertex_to_index.insert(it_find, std::make_pair(vertex, (uint32_t) vertices.size()));
                    vertices.push_back(std::move(vertex));
                }
                indices.push_back(it_find->second);
            }
        }
    }

    void initWindow() {
        FATAL_ASSERT(glfwInit(), "glfw failed to initialize.");

        glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API); // don't initialize an OpenGL context

        GLFWmonitor* primary_monitor = glfwGetPrimaryMonitor();

        int work_xpos, work_ypos, work_width, work_height;
        glfwGetMonitorWorkarea(primary_monitor, &work_xpos, &work_ypos, &work_width, &work_height);

        window = glfwCreateWindow(work_width, work_height, "Vulkan Tutorial", nullptr, nullptr);
        if (!window) {
            glfwTerminate();
            throw std::runtime_error{ "Failed to create GLFW window" };
        }

        glfwSetWindowUserPointer(window, this);
        glfwSetFramebufferSizeCallback(window, framebuffer_resize_cb);
        glfwSetWindowFocusCallback(window, window_focus_cb);

        lifecycle_state.has_input_focus = (glfwGetWindowAttrib(window, GLFW_FOCUSED) == GLFW_TRUE);
    }

    static void framebuffer_resize_cb(GLFWwindow* window, int width, int height) {
        Application* app = reinterpret_cast<Application*>(glfwGetWindowUserPointer(window));
        app->is_swapchain_dirty = true;
        app->lifecycle_state.has_graphics = (width != 0 && height != 0);
    }

    static void window_focus_cb(GLFWwindow* window, int focused) {
        Application* app = reinterpret_cast<Application*>(glfwGetWindowUserPointer(window));
        app->lifecycle_state.has_input_focus = (focused == GLFW_TRUE);
    }

    void initVulkan() {
        createInstance();
        setupDebugMessenger();

        if (glfwCreateWindowSurface(instance, window, nullptr, &reinterpret_cast<VkSurfaceKHR&>(surface)) != VK_SUCCESS) {
            throw std::runtime_error("failed to create window surface.");
        }

        device_info = createLogicalDevice();

        const std::array<vk::DescriptorPoolSize, 2> pool_sizes = {
            vk::DescriptorPoolSize{ vk::DescriptorType::eUniformBuffer, max_frames_in_flight },
            vk::DescriptorPoolSize{ vk::DescriptorType::eCombinedImageSampler, max_frames_in_flight }
        };
        descriptor_pool = device.createDescriptorPool(vk::DescriptorPoolCreateInfo{ {}, max_frames_in_flight, pool_sizes });

        const vk::DescriptorSetLayoutBinding ubo_layout_binding{ 0, vk::DescriptorType::eUniformBuffer, 1, vk::ShaderStageFlagBits::eVertex, nullptr };
        const vk::DescriptorSetLayoutBinding sample_layout_binding{ 1, vk::DescriptorType::eCombinedImageSampler, 1, vk::ShaderStageFlagBits::eFragment, nullptr };
        const std::array<vk::DescriptorSetLayoutBinding, 2> bindings = { ubo_layout_binding, sample_layout_binding };
        descriptor_set_layout = device.createDescriptorSetLayout(vk::DescriptorSetLayoutCreateInfo{ {}, bindings });

        const std::vector<vk::DescriptorSetLayout> layouts(max_frames_in_flight, descriptor_set_layout);
        const vk::DescriptorSetAllocateInfo alloc_info{ descriptor_pool, layouts };
        descriptor_sets = device.allocateDescriptorSets(alloc_info);

        command_pool = device.createCommandPool(vk::CommandPoolCreateInfo{ vk::CommandPoolCreateFlagBits::eResetCommandBuffer, device_info.graphics_family_idx });

        frame_data.resize(max_frames_in_flight);

        for (auto& frame_datum : frame_data) {
            frame_datum.command_buffer = device.allocateCommandBuffers(vk::CommandBufferAllocateInfo{ command_pool, vk::CommandBufferLevel::ePrimary, 1 }).front();
            frame_datum.image_available_sem = device.createSemaphore(vk::SemaphoreCreateInfo{});
            frame_datum.render_finished_sem = device.createSemaphore(vk::SemaphoreCreateInfo{});
            frame_datum.in_flight_fence = device.createFence(vk::FenceCreateInfo{ vk::FenceCreateFlagBits::eSignaled });

            const vk::DeviceSize buffer_size = sizeof(UniformBufferObject);
            std::tie(frame_datum.uniform_buffer, frame_datum.uniform_buffer_memory) = create_buffer(buffer_size, vk::BufferUsageFlagBits::eUniformBuffer, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent);
        }

        createTextureImage();

        for (size_t i = 0; i < max_frames_in_flight; ++i) {
            const vk::DescriptorBufferInfo buffer_info{ frame_data[i].uniform_buffer, 0, sizeof(UniformBufferObject) };
            const vk::WriteDescriptorSet buffer_descriptor_write{ descriptor_sets[i], 0, 0, 1, vk::DescriptorType::eUniformBuffer, nullptr, &buffer_info, nullptr };
            const vk::DescriptorImageInfo image_info{ texture_sampler, texture_image_view, vk::ImageLayout::eShaderReadOnlyOptimal };
            const vk::WriteDescriptorSet image_descriptor_write{ descriptor_sets[i], 1, 0, 1, vk::DescriptorType::eCombinedImageSampler, &image_info, nullptr, nullptr };
            device.updateDescriptorSets({ buffer_descriptor_write, image_descriptor_write }, nullptr);
        }

        std::tie(vertex_buffer, vertex_buffer_memory) = copyCpuDataToBuffer(vertices.data(), sizeof(Vertex) * vertices.size(), vk::BufferUsageFlagBits::eVertexBuffer);
        std::tie(index_buffer, index_buffer_memory) = copyCpuDataToBuffer(indices.data(), sizeof(uint32_t) * indices.size(), vk::BufferUsageFlagBits::eIndexBuffer);

        createSwapChain(device_info);
    }

    void createInstance() {
        const auto application_info = vk::ApplicationInfo("Hello Triangle", VK_MAKE_VERSION(1, 0, 0), nullptr, VK_MAKE_VERSION(1, 0, 0), VK_API_VERSION_1_0);

        // get required extensions
        uint32_t glfw_extension_count;
        const char** glfw_extension_names;
        glfw_extension_names = glfwGetRequiredInstanceExtensions(&glfw_extension_count);

        std::vector<const char*> required_extensions(glfw_extension_names, glfw_extension_names + glfw_extension_count);
        if (enable_validation_layers) {
            const std::vector<vk::LayerProperties> layer_properties = vk::enumerateInstanceLayerProperties();

            const bool are_validation_layers_supported = std::all_of(validation_layers.begin(), validation_layers.end(), [&layer_properties](const char* validation_layer_name) {
                return std::any_of(layer_properties.begin(), layer_properties.end(), [validation_layer_name](const vk::LayerProperties& layer) {
                    return strcmp(validation_layer_name, layer.layerName) == 0;
                    });
                });

            if (!are_validation_layers_supported)
                throw std::runtime_error{ "validation layers requested, but not available." };

            required_extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
            auto debugCreateInfo = createDebugMessengerInfo();
            instance = vk::createInstance(vk::InstanceCreateInfo{ {}, &application_info, validation_layers, required_extensions, &debugCreateInfo });
        }
        else {
            instance = vk::createInstance(vk::InstanceCreateInfo{ {}, &application_info, validation_layers, required_extensions });
        }

        const std::vector<vk::ExtensionProperties> extension_properties = vk::enumerateInstanceExtensionProperties();
        std::cout << "available extensions:\n";
        for (const auto& extension : extension_properties) {
            std::cout << '\t' << extension.extensionName << '\n';
        }
    }

    vk::DebugUtilsMessengerCreateInfoEXT createDebugMessengerInfo() {
        return vk::DebugUtilsMessengerCreateInfoEXT{ {},
                vk::DebugUtilsMessageSeverityFlagBitsEXT::eWarning | vk::DebugUtilsMessageSeverityFlagBitsEXT::eError,
                vk::DebugUtilsMessageTypeFlagBitsEXT::eGeneral | vk::DebugUtilsMessageTypeFlagBitsEXT::eValidation | vk::DebugUtilsMessageTypeFlagBitsEXT::ePerformance,
                vk_debug_callback };
    }

    void setupDebugMessenger() {
        if (enable_validation_layers) {
            pfnVkCreateDebugUtilsMessengerEXT = reinterpret_cast<PFN_vkCreateDebugUtilsMessengerEXT>(instance.getProcAddr("vkCreateDebugUtilsMessengerEXT"));
            if (!pfnVkCreateDebugUtilsMessengerEXT) {
                throw std::runtime_error{ "pfnVkCreateDebugUtilsMessengerEXT not found." };
            }
            pfnVkDestroyDebugUtilsMessengerEXT = reinterpret_cast<PFN_vkDestroyDebugUtilsMessengerEXT>(instance.getProcAddr("vkDestroyDebugUtilsMessengerEXT"));
            if (!pfnVkDestroyDebugUtilsMessengerEXT) {
                throw std::runtime_error{ "pfnVkDestroyDebugUtilsMessengerEXT not found." };
            }

            debug_messenger = instance.createDebugUtilsMessengerEXT(createDebugMessengerInfo());
        }
    }

    DeviceInfo createLogicalDevice() {
        const auto available_physical_devices = instance.enumeratePhysicalDevices();

        // select a device that supports the queue families, required extensions, formats and present modes:
        auto it_device = available_physical_devices.begin();
        DeviceInfo device_info;

        for (; it_device != available_physical_devices.end(); ++it_device) {
            const vk::PhysicalDevice& device = *it_device;
            const auto queue_families = device.getQueueFamilyProperties();

            device_info.graphics_family_idx = queue_families.size();
            device_info.present_family_idx = queue_families.size();

            auto has_required_queue_families = [&]() {
                return (device_info.graphics_family_idx != queue_families.size() &&
                    device_info.present_family_idx != queue_families.size());
            };

            const auto extension_properties = device.enumerateDeviceExtensionProperties();

            for (auto family_idx = 0; !has_required_queue_families() && family_idx < queue_families.size(); ++family_idx) {
                const vk::QueueFamilyProperties& queue_family = queue_families[family_idx];

                bool has_graphics_queue = (queue_family.queueFlags & vk::QueueFlagBits::eGraphics) == vk::QueueFlagBits::eGraphics;
                if (has_graphics_queue)
                    device_info.graphics_family_idx = family_idx;

                bool has_present_support = device.getSurfaceSupportKHR(family_idx, surface);
                if (has_present_support)
                    device_info.present_family_idx = family_idx;
            }

            if (!has_required_queue_families())
                continue;
            
            const bool has_required_extensions = std::all_of(required_device_extensions.begin(), required_device_extensions.end(), [&extension_properties](const char* required_extension) {
                return std::any_of(extension_properties.begin(), extension_properties.end(), [required_extension](const vk::ExtensionProperties& properties) {
                    return strcmp(properties.extensionName, required_extension) == 0;
                });
            });

            if (!has_required_extensions)
                continue;

            // following queries require the extensions we just checked.
            // select best settings for creating the swapchain based on what's supported:

            auto avilable_formats = device.getSurfaceFormatsKHR(surface);
            if (avilable_formats.empty())
                continue;

            static constexpr vk::SurfaceFormatKHR preferred_format{ vk::Format::eB8G8R8A8Srgb, vk::ColorSpaceKHR::eSrgbNonlinear };
            device_info.format = xtd::contains(avilable_formats, preferred_format) ? preferred_format
                : avilable_formats.front();

            auto available_present_modes = device.getSurfacePresentModesKHR(surface);
            if (available_present_modes.empty())
                continue;

            const auto supported_features = device.getFeatures();
            if (!supported_features.samplerAnisotropy)
                continue;

            device_info.present_mode = xtd::contains(available_present_modes, vk::PresentModeKHR::eMailbox) ? vk::PresentModeKHR::eMailbox
                : vk::PresentModeKHR::eFifo;

            break; // select this device
        }

        if (it_device == available_physical_devices.end()) {
            throw std::runtime_error{ "no suitable physical device found." };
        }

        physical_device = *it_device;

        const auto device_properties = physical_device.getProperties();
        std::cout << "Selected device: " << device_properties.deviceName << " (" << to_string(device_properties.deviceType) << ')' << std::endl;

        const auto queue_families = physical_device.getQueueFamilyProperties();
        if (!queue_families.empty()) {
            std::cout << "Queues available:\n";
            for (const auto& queue_family : queue_families) {
                std::cout << '\t' << queue_family.queueCount << "x " << to_string(queue_family.queueFlags) << std::endl;
            }
        }

        static const std::array<vk::Format, 3> candidate_formats = { vk::Format::eD32Sfloat, vk::Format::eD32SfloatS8Uint, vk::Format::eD24UnormS8Uint }; // what about vk::Format::eX8D24UnormPack32 ?
        auto it_depth_format = std::find_if(candidate_formats.begin(), candidate_formats.end(), [this](vk::Format format) {
            auto format_props = physical_device.getFormatProperties(format);
            return (format_props.optimalTilingFeatures & vk::FormatFeatureFlagBits::eDepthStencilAttachment) == vk::FormatFeatureFlagBits::eDepthStencilAttachment;
        });

        if (it_depth_format == candidate_formats.end())
            throw std::runtime_error{ "failed to find supported depth format." };

        depth_format = *it_depth_format;

        // create the device and queues
        vk::PhysicalDeviceFeatures device_features;
        device_features.samplerAnisotropy = VK_TRUE;
        const auto queue_priorities = { 1.0f };
        const std::set<uint32_t> unique_queue_family_indices{ device_info.graphics_family_idx, device_info.present_family_idx };
        std::vector<vk::DeviceQueueCreateInfo> queue_create_infos;
        queue_create_infos.reserve(unique_queue_family_indices.size());
        for (uint32_t queue_family_idx : unique_queue_family_indices)
            queue_create_infos.push_back(vk::DeviceQueueCreateInfo{ {}, queue_family_idx, queue_priorities });
        device = physical_device.createDevice(vk::DeviceCreateInfo{ {}, queue_create_infos, validation_layers, required_device_extensions, &device_features });
        graphics_queue = device.getQueue(device_info.graphics_family_idx, 0);
        presentation_queue = device.getQueue(device_info.present_family_idx, 0);

        vk::SampleCountFlags supported_sample_counts = device_properties.limits.framebufferColorSampleCounts & device_properties.limits.framebufferDepthSampleCounts;
        if (supported_sample_counts & vk::SampleCountFlagBits::e64)
            msaa_samples = vk::SampleCountFlagBits::e64;
        else if (supported_sample_counts & vk::SampleCountFlagBits::e32)
            msaa_samples = vk::SampleCountFlagBits::e32;
        else if (supported_sample_counts & vk::SampleCountFlagBits::e16)
            msaa_samples = vk::SampleCountFlagBits::e16;
        else if (supported_sample_counts & vk::SampleCountFlagBits::e8)
            msaa_samples = vk::SampleCountFlagBits::e8;
        else if (supported_sample_counts & vk::SampleCountFlagBits::e4)
            msaa_samples = vk::SampleCountFlagBits::e4;
        else if (supported_sample_counts & vk::SampleCountFlagBits::e2)
            msaa_samples = vk::SampleCountFlagBits::e2;
        else
            msaa_samples = vk::SampleCountFlagBits::e1;

        return device_info;
    }

    void createTextureImage() {
        int tex_width, tex_height, num_channels;
        stbi_uc* pixels = stbi_load(texture_path, &tex_width, &tex_height, &num_channels, STBI_rgb_alpha);
        vk::DeviceSize image_size = tex_width * tex_height * 4;
        mip_levels = std::floor(std::log2(std::max(tex_width, tex_height))) + 1;

        if (!pixels)
            throw std::runtime_error{ "failed to load texture image." };

        auto [staging_buffer, staging_buffer_memory] = create_buffer(image_size, vk::BufferUsageFlagBits::eTransferSrc, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent);
        void* data = device.mapMemory(staging_buffer_memory, 0, image_size, {});
        std::memcpy(data, pixels, (size_t) image_size);
        device.unmapMemory(staging_buffer_memory);

        stbi_image_free(pixels);

        vk::Extent3D extent{ (uint32_t) tex_width, (uint32_t) tex_height, 1 };
        // todo: validate that vk::Format::eR8G8B8A8Srgb is supported by the device.
        static const vk::Format image_format = vk::Format::eR8G8B8A8Srgb;
        std::tie(texture_image, texture_image_memory) = createImage(extent, mip_levels, vk::SampleCountFlagBits::e1, image_format, vk::ImageTiling::eOptimal,
            vk::ImageUsageFlagBits::eTransferSrc | vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eSampled, vk::MemoryPropertyFlagBits::eDeviceLocal);

        transitionImageLayout(texture_image, image_format, mip_levels, false, vk::ImageLayout::eUndefined, vk::ImageLayout::eTransferDstOptimal,
            {}, vk::AccessFlagBits::eTransferWrite, vk::PipelineStageFlagBits::eTopOfPipe, vk::PipelineStageFlagBits::eTransfer);
        copyBufferToImage(staging_buffer, texture_image, extent);

        // generate mip maps
        // normally this is baked data.

        auto format_props = physical_device.getFormatProperties(image_format);
        if (!(format_props.optimalTilingFeatures & vk::FormatFeatureFlagBits::eSampledImageFilterLinear))
            throw std::runtime_error{ "texture image format does not support linear blitting." };

        auto command_buffer = beginSingleTimeCommands();
        auto create_image_barrier = [this](uint32_t base_miplevel, vk::AccessFlags src_access, vk::AccessFlags dst_access, vk::ImageLayout old_Layout, vk::ImageLayout new_layout) {
            return vk::ImageMemoryBarrier{ src_access, dst_access, old_Layout, new_layout, VK_QUEUE_FAMILY_IGNORED, VK_QUEUE_FAMILY_IGNORED, texture_image, vk::ImageSubresourceRange{ vk::ImageAspectFlagBits::eColor, base_miplevel, 1, 0, 1 } };
        };

        int32_t mip_width = tex_width, mip_height = tex_height;
        for (uint32_t mip_level = 1; mip_level < mip_levels; ++mip_level) {
            const auto barrier_src_optimal = create_image_barrier(mip_level - 1, vk::AccessFlagBits::eTransferWrite, vk::AccessFlagBits::eTransferRead, vk::ImageLayout::eTransferDstOptimal, vk::ImageLayout::eTransferSrcOptimal);
            command_buffer.pipelineBarrier(vk::PipelineStageFlagBits::eTransfer, vk::PipelineStageFlagBits::eTransfer, {}, nullptr, nullptr, barrier_src_optimal);

            auto new_mip_width = (mip_width > 1) ? (mip_width / 2) : mip_width;
            auto new_mip_height = (mip_height > 1) ? (mip_height / 2) : mip_height;

            const vk::ImageBlit blit{ vk::ImageSubresourceLayers{ vk::ImageAspectFlagBits::eColor, mip_level - 1, 0, 1 },
                std::array<vk::Offset3D, 2>{
                    vk::Offset3D{ 0, 0, 0 },
                    vk::Offset3D{ mip_width, mip_height, 1 }
                },
                vk::ImageSubresourceLayers{ vk::ImageAspectFlagBits::eColor, mip_level, 0, 1 },
                std::array<vk::Offset3D, 2>{
                    vk::Offset3D{ 0, 0, 0 },
                    vk::Offset3D{ new_mip_width, new_mip_height, 1 }
                }
            };
            command_buffer.blitImage(texture_image, vk::ImageLayout::eTransferSrcOptimal, texture_image, vk::ImageLayout::eTransferDstOptimal, blit, vk::Filter::eLinear);

            const auto barrier_shader_ro_optimal = create_image_barrier(mip_level - 1, vk::AccessFlagBits::eTransferRead, vk::AccessFlagBits::eShaderRead, vk::ImageLayout::eTransferSrcOptimal, vk::ImageLayout::eShaderReadOnlyOptimal);
            command_buffer.pipelineBarrier(vk::PipelineStageFlagBits::eTransfer, vk::PipelineStageFlagBits::eFragmentShader, {}, nullptr, nullptr, barrier_shader_ro_optimal);

            mip_width = new_mip_width;
            mip_height = new_mip_height;
        }

        const auto barrier_last_mip_shader_ro_optimal = create_image_barrier(mip_levels - 1, vk::AccessFlagBits::eTransferWrite, vk::AccessFlagBits::eShaderRead, vk::ImageLayout::eTransferDstOptimal, vk::ImageLayout::eShaderReadOnlyOptimal);
        command_buffer.pipelineBarrier(vk::PipelineStageFlagBits::eTransfer, vk::PipelineStageFlagBits::eFragmentShader, {}, nullptr, nullptr, barrier_last_mip_shader_ro_optimal);

        endSingleTimeCommands(command_buffer);

        device.destroyBuffer(staging_buffer);
        device.freeMemory(staging_buffer_memory);

        texture_image_view = device.createImageView(vk::ImageViewCreateInfo{
            {}, texture_image, vk::ImageViewType::e2D, image_format, {}, vk::ImageSubresourceRange{
                vk::ImageAspectFlagBits::eColor, 0, mip_levels, 0, 1
            }
            });

        const auto device_properties = physical_device.getProperties();
        const auto max_anisotropy = std::min(8.0f, device_properties.limits.maxSamplerAnisotropy);

        texture_sampler = device.createSampler(vk::SamplerCreateInfo{ {}, vk::Filter::eLinear, vk::Filter::eLinear, vk::SamplerMipmapMode::eLinear, vk::SamplerAddressMode::eRepeat, vk::SamplerAddressMode::eRepeat, vk::SamplerAddressMode::eRepeat,
        0.0f, VK_TRUE, max_anisotropy, VK_FALSE, vk::CompareOp::eNever, 0.0f, (float) mip_levels, vk::BorderColor::eIntOpaqueBlack, VK_FALSE });
    }

    std::pair<vk::Image, vk::DeviceMemory> createImage(vk::Extent3D extent, uint32_t mip_levels, vk::SampleCountFlagBits samples, vk::Format format, vk::ImageTiling tiling, vk::ImageUsageFlags usage, vk::MemoryPropertyFlags properties) {
        vk::ImageCreateInfo image_create{ {}, vk::ImageType::e2D, format, extent, mip_levels, 1, samples, tiling,
            usage, vk::SharingMode::eExclusive, nullptr, vk::ImageLayout::eUndefined };
        auto texture_image = device.createImage(image_create);

        vk::MemoryRequirements memory_req = device.getImageMemoryRequirements(texture_image);
        vk::MemoryAllocateInfo alloc_info{ memory_req.size, find_memory_type(memory_req.memoryTypeBits, properties) };
        auto texture_image_memory = device.allocateMemory(alloc_info);
        
        device.bindImageMemory(texture_image, texture_image_memory, 0);

        return std::make_pair(texture_image, texture_image_memory);
    }

    void copyBufferToImage(vk::Buffer buffer, vk::Image image, vk::Extent3D extent) {
        vk::CommandBuffer command_buffer = beginSingleTimeCommands();

        vk::BufferImageCopy region{ 0, 0, 0, vk::ImageSubresourceLayers{ vk::ImageAspectFlagBits::eColor, 0, 0, 1 }, vk::Offset3D{ 0, 0, 0 }, extent };
        command_buffer.copyBufferToImage(buffer, image, vk::ImageLayout::eTransferDstOptimal, region );

        endSingleTimeCommands(command_buffer);
    }

    bool hasStencilComponent(vk::Format format) {
        return (format == vk::Format::eS8Uint ||
            format == vk::Format::eD16UnormS8Uint ||
            format == vk::Format::eD24UnormS8Uint ||
            format == vk::Format::eD32SfloatS8Uint);
    }

    void transitionImageLayout(vk::Image image, vk::Format format, uint32_t mip_levels, bool is_depth, vk::ImageLayout old_layout, vk::ImageLayout new_layout,
        vk::AccessFlags src_access, vk::AccessFlags dst_access, vk::PipelineStageFlags src_stage, vk::PipelineStageFlags dst_stage) {
        vk::CommandBuffer command_buffer = beginSingleTimeCommands();

        vk::ImageAspectFlags image_aspect_flags = (is_depth ? vk::ImageAspectFlagBits::eDepth : vk::ImageAspectFlagBits::eColor) | (hasStencilComponent(format) ? vk::ImageAspectFlagBits::eStencil : vk::ImageAspectFlagBits::eNone);
        vk::ImageMemoryBarrier barrier{ src_access, dst_access, old_layout, new_layout, VK_QUEUE_FAMILY_IGNORED, VK_QUEUE_FAMILY_IGNORED,
            image, vk::ImageSubresourceRange{ is_depth ? vk::ImageAspectFlagBits::eDepth : vk::ImageAspectFlagBits::eColor, 0, mip_levels, 0, 1 } };
        command_buffer.pipelineBarrier(src_stage, dst_stage, {}, nullptr, nullptr, barrier);

        endSingleTimeCommands(command_buffer);
    }

    void createSwapChain(const DeviceInfo& device_info) {
        vk::SurfaceCapabilitiesKHR capabilities = physical_device.getSurfaceCapabilitiesKHR(surface);

        vk::Extent2D chosen_extent;
        if (capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max()) {
            chosen_extent = capabilities.currentExtent;
        }
        else {
            int pixel_width, pixel_height;
            glfwGetFramebufferSize(window, &pixel_width, &pixel_height);

            chosen_extent = vk::Extent2D{
                std::clamp((uint32_t) pixel_width, capabilities.minImageExtent.width, capabilities.maxImageExtent.width),
                std::clamp((uint32_t) pixel_height, capabilities.minImageExtent.height, capabilities.maxImageExtent.height)
            };
        }

        lifecycle_state.has_graphics = (chosen_extent.width != 0 && chosen_extent.height != 0);
        if (!lifecycle_state.has_graphics)
            return;

        static constexpr uint32_t preferred_swapchain_image_count = 3;
        // capabilities.maxImageCount == 0 means there is no maximum.
        const uint32_t min_swapchain_image_count = capabilities.maxImageCount > 0 ? std::clamp(preferred_swapchain_image_count, capabilities.minImageCount, capabilities.maxImageCount)
            : std::max(preferred_swapchain_image_count, capabilities.minImageCount);

        vk::SharingMode sharing_mode;
        std::vector<uint32_t> queue_family_indices;

        if (device_info.graphics_family_idx != device_info.present_family_idx) {
            sharing_mode = vk::SharingMode::eConcurrent;
            queue_family_indices.push_back(device_info.graphics_family_idx);
            queue_family_indices.push_back(device_info.present_family_idx);
        }
        else {
            sharing_mode = vk::SharingMode::eExclusive;
        }

        swapchain = device.createSwapchainKHR(vk::SwapchainCreateInfoKHR{ {}, surface, min_swapchain_image_count, device_info.format.format, device_info.format.colorSpace, chosen_extent,
            1, vk::ImageUsageFlagBits::eColorAttachment,
            sharing_mode, queue_family_indices,
            vk::SurfaceTransformFlagBitsKHR::eIdentity, vk::CompositeAlphaFlagBitsKHR::eOpaque,
            device_info.present_mode,
            VK_TRUE,
            VK_NULL_HANDLE
        });

        swapchain_images = device.getSwapchainImagesKHR(swapchain);
        swapchain_image_format = device_info.format.format;
        swapchain_extent = chosen_extent;

        swapchain_image_views.reserve(swapchain_images.size());
        for (const vk::Image& image : swapchain_images) {
            swapchain_image_views.push_back(device.createImageView(vk::ImageViewCreateInfo{
                {}, image, vk::ImageViewType::e2D, swapchain_image_format, {}, vk::ImageSubresourceRange{
                    vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1
                }
            }));
        }

        createRenderPass();
        createGraphicsPipeline();

        // create color image resources
        const auto color_format = swapchain_image_format;
        std::tie(color_image, color_image_memory) = createImage(vk::Extent3D{ swapchain_extent.width, swapchain_extent.height, 1 }, 1, msaa_samples, color_format,
            vk::ImageTiling::eOptimal, vk::ImageUsageFlagBits::eTransientAttachment | vk::ImageUsageFlagBits::eColorAttachment, vk::MemoryPropertyFlagBits::eDeviceLocal);
        color_image_view = device.createImageView(vk::ImageViewCreateInfo{
            {}, color_image, vk::ImageViewType::e2D, color_format, {}, vk::ImageSubresourceRange{
                vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1
            }
            });

        // create depth image resources
        std::tie(depth_image, depth_image_memory) = createImage(vk::Extent3D{ swapchain_extent.width, swapchain_extent.height, 1 }, 1, msaa_samples, depth_format,
            vk::ImageTiling::eOptimal, vk::ImageUsageFlagBits::eDepthStencilAttachment, vk::MemoryPropertyFlagBits::eDeviceLocal);

        depth_image_view = device.createImageView(vk::ImageViewCreateInfo{
            {}, depth_image, vk::ImageViewType::e2D, depth_format, {}, vk::ImageSubresourceRange{
                vk::ImageAspectFlagBits::eDepth, 0, 1, 0, 1
            }
            });

        transitionImageLayout(depth_image, depth_format, 1, true, vk::ImageLayout::eUndefined, vk::ImageLayout::eDepthStencilAttachmentOptimal,
            {}, vk::AccessFlagBits::eTransferWrite, vk::PipelineStageFlagBits::eTopOfPipe, vk::PipelineStageFlagBits::eTransfer);

        // create framebuffers
        swapchain_framebuffers.resize(swapchain_image_views.size());

        for (size_t i = 0; i < swapchain_image_views.size(); ++i) {
            const std::array<vk::ImageView, 3> attachments{ color_image_view, depth_image_view, swapchain_image_views[i] };
            swapchain_framebuffers[i] = device.createFramebuffer(vk::FramebufferCreateInfo{ {}, render_pass, attachments, swapchain_extent.width, swapchain_extent.height, 1 });
        }
    }

    void createRenderPass() {
        const vk::AttachmentDescription color_attachment{ {}, swapchain_image_format, msaa_samples,
            vk::AttachmentLoadOp::eClear, vk::AttachmentStoreOp::eStore,
            vk::AttachmentLoadOp::eDontCare, vk::AttachmentStoreOp::eDontCare,
            vk::ImageLayout::eUndefined, vk::ImageLayout::eColorAttachmentOptimal
        };
        const vk::AttachmentReference color_attachment_ref{ 0, vk::ImageLayout::eColorAttachmentOptimal };

        const vk::AttachmentDescription depth_attachment{ {}, depth_format, msaa_samples,
            vk::AttachmentLoadOp::eClear, vk::AttachmentStoreOp::eDontCare,
            vk::AttachmentLoadOp::eDontCare, vk::AttachmentStoreOp::eDontCare,
            vk::ImageLayout::eUndefined, vk::ImageLayout::eDepthStencilAttachmentOptimal
        };
        const vk::AttachmentReference depth_attachment_ref{ 1, vk::ImageLayout::eDepthStencilAttachmentOptimal };

        const vk::AttachmentDescription color_resolve_attachment{ {}, swapchain_image_format, vk::SampleCountFlagBits::e1,
            vk::AttachmentLoadOp::eDontCare, vk::AttachmentStoreOp::eStore,
            vk::AttachmentLoadOp::eDontCare, vk::AttachmentStoreOp::eDontCare,
            vk::ImageLayout::eUndefined, vk::ImageLayout::ePresentSrcKHR
        };
        const vk::AttachmentReference color_resolve_attachment_ref{ 2, vk::ImageLayout::eColorAttachmentOptimal };

        const vk::SubpassDescription subpass_desc{ {}, vk::PipelineBindPoint::eGraphics, nullptr, color_attachment_ref, color_resolve_attachment_ref, &depth_attachment_ref, nullptr };

        const vk::SubpassDependency dependency{ VK_SUBPASS_EXTERNAL, 0,
            vk::PipelineStageFlagBits::eColorAttachmentOutput | vk::PipelineStageFlagBits::eEarlyFragmentTests,
            vk::PipelineStageFlagBits::eColorAttachmentOutput | vk::PipelineStageFlagBits::eEarlyFragmentTests,
            {}, vk::AccessFlagBits::eColorAttachmentWrite | vk::AccessFlagBits::eDepthStencilAttachmentWrite,
            {}
        };

        const std::array<vk::AttachmentDescription, 3> attachments{ color_attachment, depth_attachment, color_resolve_attachment };
        render_pass = device.createRenderPass(vk::RenderPassCreateInfo{ {}, attachments, subpass_desc, dependency });
    }

    void createGraphicsPipeline() {
        auto vert_module = compileShaderModule("shaders/basic.vert", shaderc_vertex_shader);
        auto frag_module = compileShaderModule("shaders/basic.frag", shaderc_fragment_shader);

        const std::array<vk::PipelineShaderStageCreateInfo, 2> shader_stages = {
            vk::PipelineShaderStageCreateInfo{ {}, vk::ShaderStageFlagBits::eVertex, vert_module, "main" },
            vk::PipelineShaderStageCreateInfo{ {}, vk::ShaderStageFlagBits::eFragment, frag_module, "main" }
        };

        const vk::PipelineVertexInputStateCreateInfo vertex_input_info{ {}, Vertex::BindingDescription, Vertex::AttributeDescriptions };

        const vk::PipelineInputAssemblyStateCreateInfo input_assembly_info{ {}, vk::PrimitiveTopology::eTriangleList, VK_FALSE };

        const vk::PipelineTessellationStateCreateInfo tesselation_info{};

        const vk::Viewport viewport{ 0.0f, 0.0f, (float) swapchain_extent.width, (float) swapchain_extent.height, 0.0f, 1.0f };
        const vk::Rect2D scissor{ vk::Offset2D{ 0, 0 }, swapchain_extent };

        const vk::PipelineViewportStateCreateInfo viewport_info{ {}, viewport, scissor };

        const vk::PipelineRasterizationStateCreateInfo rasterizer_info{ {}, VK_FALSE, VK_FALSE, vk::PolygonMode::eFill, vk::CullModeFlagBits::eBack, vk::FrontFace::eCounterClockwise, VK_FALSE, 0.0f, 0.0f, 0.0f, 1.0f };

        const vk::PipelineMultisampleStateCreateInfo multisample_info{ {}, msaa_samples, VK_FALSE, 1.0f, nullptr, VK_FALSE, VK_FALSE };

        const vk::PipelineDepthStencilStateCreateInfo depth_stencil_info{
            {}, VK_TRUE, VK_TRUE, vk::CompareOp::eLess,
            VK_FALSE, VK_FALSE, {}, {}, 0.0f, 1.0f
        };

        const vk::PipelineColorBlendAttachmentState color_blend_attachment{
            VK_FALSE,
            vk::BlendFactor::eOne, vk::BlendFactor::eZero, vk::BlendOp::eAdd,
            vk::BlendFactor::eOne, vk::BlendFactor::eZero, vk::BlendOp::eAdd,
            vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA
        };

        const std::array<float, 4> blend_constants{ 0.0f, 0.0f, 0.0f, 0.0f };

        const vk::PipelineColorBlendStateCreateInfo color_blending_info{ {}, VK_FALSE, vk::LogicOp::eCopy, color_blend_attachment, blend_constants };

        const std::array<vk::DynamicState, 0> dynamic_states{};
        const vk::PipelineDynamicStateCreateInfo dynamic_info{ {}, dynamic_states };

        const vk::PipelineLayoutCreateInfo pipeline_layout_info{ {}, descriptor_set_layout };

        graphics_pipeline_layout = device.createPipelineLayout(pipeline_layout_info);

        const vk::GraphicsPipelineCreateInfo graphics_pipeline_info{ {}, shader_stages,
            &vertex_input_info, &input_assembly_info, &tesselation_info, &viewport_info, &rasterizer_info, &multisample_info, &depth_stencil_info, &color_blending_info, &dynamic_info,
            graphics_pipeline_layout, render_pass, 0,
            VK_NULL_HANDLE, 0
        };

        auto pipeline_result = device.createGraphicsPipeline(VK_NULL_HANDLE, graphics_pipeline_info);
        switch (pipeline_result.result) {
        case vk::Result::eSuccess:
            graphics_pipeline = pipeline_result.value;
            break;
        default:
            throw std::runtime_error{"failed to create graphics pipeline."};
        }

        device.destroyShaderModule(vert_module);
        device.destroyShaderModule(frag_module);
    }

    vk::ShaderModule compileShaderModule(const char* path, shaderc_shader_kind shader_kind) {
        std::string shader_source = load_shader_from_file(path);
        auto spv = compile_glsl_to_spv(shader_source, shader_kind, path);
        return device.createShaderModule(vk::ShaderModuleCreateInfo{ {}, sizeof(shaderc::SpvCompilationResult::element_type) * static_cast<size_t>(spv.end() - spv.begin()), spv.begin() });
    }

    std::pair<vk::Buffer, vk::DeviceMemory> copyCpuDataToBuffer(const void* src_data, vk::DeviceSize buffer_size, vk::BufferUsageFlags usage) {
        auto [staging_buffer, staging_buffer_memory] = create_buffer(buffer_size, vk::BufferUsageFlagBits::eTransferSrc,
            vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent);

        void* staged_data = device.mapMemory(staging_buffer_memory, 0, buffer_size, {});
        std::memcpy(staged_data, src_data, (size_t) buffer_size);
        device.unmapMemory(staging_buffer_memory);

        auto [dst_buffer, dst_buffer_memory] = create_buffer(buffer_size, vk::BufferUsageFlagBits::eTransferDst | usage, vk::MemoryPropertyFlagBits::eDeviceLocal);

        copy_buffer(staging_buffer, dst_buffer, buffer_size);

        device.destroyBuffer(staging_buffer);
        device.freeMemory(staging_buffer_memory);

        return std::make_pair(dst_buffer, dst_buffer_memory);
    }

    std::pair<vk::Buffer, vk::DeviceMemory> create_buffer(vk::DeviceSize size, vk::BufferUsageFlags usage, vk::MemoryPropertyFlags properties) {
        const vk::BufferCreateInfo buffer_info{ {}, size, usage, vk::SharingMode::eExclusive };
        auto buffer = device.createBuffer(buffer_info);

        vk::MemoryRequirements memory_req = device.getBufferMemoryRequirements(buffer);
        vk::MemoryAllocateInfo alloc_info{ memory_req.size, find_memory_type(memory_req.memoryTypeBits, properties) };
        auto buffer_memory = device.allocateMemory(alloc_info); // see https://github.com/GPUOpen-LibrariesAndSDKs/VulkanMemoryAllocator
        device.bindBufferMemory(buffer, buffer_memory, 0);

        return std::make_pair(buffer, buffer_memory);
    }

    uint32_t find_memory_type(uint32_t type_filter, vk::MemoryPropertyFlags properties) {
        vk::PhysicalDeviceMemoryProperties memory_props = physical_device.getMemoryProperties();
        for (uint32_t i = 0; i < memory_props.memoryTypeCount; ++i) {
            if ((type_filter & (1 << i)) != 0 && (memory_props.memoryTypes[i].propertyFlags & properties) == properties) {
                return i;
            }
        }

        throw std::runtime_error{ "failed to find suitable memory type." };
    }

    void copy_buffer(vk::Buffer src_buffer, vk::Buffer dst_buffer, vk::DeviceSize buffer_size) {
        vk::CommandBuffer command_buffer = beginSingleTimeCommands();

        vk::BufferCopy copy_region{ 0, 0, buffer_size };
        command_buffer.copyBuffer(src_buffer, dst_buffer, 1, &copy_region);

        endSingleTimeCommands(command_buffer);
    }

    vk::CommandBuffer beginSingleTimeCommands() {
        vk::CommandBuffer command_buffer = device.allocateCommandBuffers(vk::CommandBufferAllocateInfo{ command_pool, vk::CommandBufferLevel::ePrimary, 1 }).front();
        command_buffer.begin(vk::CommandBufferBeginInfo{ vk::CommandBufferUsageFlagBits::eOneTimeSubmit });
        return command_buffer;
    }

    void endSingleTimeCommands(vk::CommandBuffer command_buffer) {
        command_buffer.end();
        graphics_queue.submit(vk::SubmitInfo{ nullptr, nullptr, command_buffer }, VK_NULL_HANDLE);
        graphics_queue.waitIdle();
        device.freeCommandBuffers(command_pool, command_buffer);
    }

    void mainLoop() {
        while (!glfwWindowShouldClose(window)) {
            glfwPollEvents();
            if (lifecycle_state.has_graphics) {
                if(lifecycle_state.has_input_focus)
                    drawFrame();
                else
                    glfwWaitEvents();
            }
            else {
                device.waitIdle();
                cleanupSwapchain();
                glfwWaitEvents();
            }
        }

        device.waitIdle();
    }

    void drawFrame() {
        const FrameData& frame_datum = frame_data[frame_data_idx];

        VK_ASSERT(device.waitForFences(frame_datum.in_flight_fence, VK_TRUE, std::numeric_limits<uint64_t>::max()), "fence timeout");
        device.resetFences(frame_datum.in_flight_fence);

        uint32_t image_idx;

        auto reacquireSwapchainAndImage = [this, &frame_datum]() -> uint32_t {
            device.waitIdle();
            cleanupSwapchain();
            createSwapChain(device_info);

            auto acquire_result = device.acquireNextImageKHR(swapchain, std::numeric_limits<uint64_t>::max(), frame_datum.image_available_sem, VK_NULL_HANDLE);
            VK_ASSERT(acquire_result.result, "unable to acquire swapchain image.");

            return acquire_result.value;
        };

        if (is_swapchain_dirty) {
            is_swapchain_dirty = false;
            image_idx = reacquireSwapchainAndImage();
        }
        else {
            auto acquire_result = device.acquireNextImageKHR(swapchain, std::numeric_limits<uint64_t>::max(), frame_datum.image_available_sem, VK_NULL_HANDLE);
            switch (acquire_result.result) {
            case vk::Result::eSuccess:
                image_idx = acquire_result.value;
                break;
            case vk::Result::eSuboptimalKHR:
            case vk::Result::eErrorOutOfDateKHR:
                image_idx = reacquireSwapchainAndImage();
                break;
            default:
                throw std::runtime_error{ "unable to acquire swapchain image." };
            }
        }

        auto current_time = std::chrono::high_resolution_clock::now();
        float time = std::chrono::duration<float, std::chrono::seconds::period>(current_time - start_time).count();

        UniformBufferObject ubo;
        ubo.model = glm::rotate(glm::mat4(1.0f), time * glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        ubo.view = glm::lookAt(glm::vec3(2.0f, 2.0f, 2.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        ubo.proj = glm::perspective(glm::radians(45.0f), swapchain_extent.width / (float) swapchain_extent.height, 0.1f, 10.0f);
        ubo.proj[1][1] *= -1.0f; // invert the Y axis.

        // todo: use push constants to pass small buffers instead.
        void* data = device.mapMemory(frame_datum.uniform_buffer_memory, 0, sizeof(UniformBufferObject), {});
        std::memcpy(data, &ubo, sizeof(UniformBufferObject));
        device.unmapMemory(frame_datum.uniform_buffer_memory);

        recordCommandBuffer(frame_datum.command_buffer, image_idx);

        const std::array<vk::PipelineStageFlags, 1> wait_stages{ vk::PipelineStageFlagBits::eColorAttachmentOutput };
        const vk::SubmitInfo submit_info{ frame_datum.image_available_sem, wait_stages, frame_datum.command_buffer, frame_datum.render_finished_sem };
        graphics_queue.submit(submit_info, frame_datum.in_flight_fence);

        VK_ASSERT(presentation_queue.presentKHR(vk::PresentInfoKHR{ frame_datum.render_finished_sem, swapchain, image_idx, nullptr }), "presentation failed");

        frame_data_idx = (frame_data_idx + 1) % max_frames_in_flight;
        ++frame_num;
    }

    void recordCommandBuffer(vk::CommandBuffer command_buffer, uint32_t image_idx) {
        // todo: vk::CommandBufferUsageFlagBits::eOneTimeSubmit?
        // implicitly resets the command (as the command pool uses eResetCommandBuffer)
        command_buffer.begin(vk::CommandBufferBeginInfo{ {}, nullptr });
        const vk::ClearColorValue clear_color{ std::array<float, 4>{ 0.0f, 0.0f, 0.0f, 1.0f } };
        const vk::ClearDepthStencilValue clear_depth{ 1.0f, 0 };
        const std::array<vk::ClearValue, 2> clear_values{ clear_color, clear_depth };
        command_buffer.beginRenderPass(vk::RenderPassBeginInfo{ render_pass, swapchain_framebuffers[image_idx], vk::Rect2D{ vk::Offset2D{ 0, 0 }, swapchain_extent }, clear_values }, vk::SubpassContents::eInline);
        command_buffer.bindPipeline(vk::PipelineBindPoint::eGraphics, graphics_pipeline);
        const std::array<vk::DeviceSize, 1> offsets{ 0 };
        command_buffer.bindVertexBuffers(0, vertex_buffer, offsets);
        command_buffer.bindIndexBuffer(index_buffer, 0, vk::IndexType::eUint32);
        command_buffer.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, graphics_pipeline_layout, 0, descriptor_sets[frame_data_idx], nullptr);
        command_buffer.drawIndexed((uint32_t) indices.size(), 1, 0, 0, 0);
        command_buffer.endRenderPass();
        command_buffer.end();
    }

    void cleanupSwapchain() {
        for (vk::Framebuffer framebuffer : swapchain_framebuffers) {
            device.destroyFramebuffer(framebuffer);
        }
        swapchain_framebuffers.clear();

        device.destroyPipeline(graphics_pipeline);
        graphics_pipeline = VK_NULL_HANDLE;
        device.destroyPipelineLayout(graphics_pipeline_layout);
        graphics_pipeline_layout = VK_NULL_HANDLE;
        device.destroyRenderPass(render_pass);
        render_pass = VK_NULL_HANDLE;

        for (const vk::ImageView& image_view : swapchain_image_views) {
            device.destroyImageView(image_view);
        }
        swapchain_image_views.clear();

        device.destroySwapchainKHR(swapchain);
        swapchain = VK_NULL_HANDLE;

        device.destroyImageView(color_image_view);
        color_image_view = VK_NULL_HANDLE;
        device.destroyImage(color_image);
        color_image = VK_NULL_HANDLE;
        device.freeMemory(color_image_memory);
        color_image_memory = VK_NULL_HANDLE;

        device.destroyImageView(depth_image_view);
        depth_image_view = VK_NULL_HANDLE;
        device.destroyImage(depth_image);
        depth_image = VK_NULL_HANDLE;
        device.freeMemory(depth_image_memory);
        depth_image_memory = VK_NULL_HANDLE;
    }

    void cleanup() {
        device.destroySampler(texture_sampler);
        device.destroyImageView(texture_image_view);
        device.destroyImage(texture_image);
        device.freeMemory(texture_image_memory);
        device.destroyBuffer(vertex_buffer);
        device.freeMemory(vertex_buffer_memory);
        device.destroyBuffer(index_buffer);
        device.freeMemory(index_buffer_memory);

        for (const FrameData& frame_datum : frame_data) {
            device.destroySemaphore(frame_datum.image_available_sem);
            device.destroySemaphore(frame_datum.render_finished_sem);
            device.destroyFence(frame_datum.in_flight_fence);
            device.destroyBuffer(frame_datum.uniform_buffer);
            device.freeMemory(frame_datum.uniform_buffer_memory);
        }

        device.destroyCommandPool(command_pool);

        cleanupSwapchain();

        device.destroyDescriptorSetLayout(descriptor_set_layout);
        device.destroyDescriptorPool(descriptor_pool);

        device.destroy();

        vkDestroySurfaceKHR(instance, surface, nullptr);

        if (enable_validation_layers) {
            instance.destroyDebugUtilsMessengerEXT(debug_messenger);
        }

        instance.destroy();

        glfwDestroyWindow(window);

        glfwTerminate();
    }

    static VKAPI_ATTR VkBool32 VKAPI_CALL vk_debug_callback(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity, VkDebugUtilsMessageTypeFlagsEXT messageType, const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData, void* pUserData) {
        std::cerr << "Vulkan validation: " << pCallbackData->pMessage << std::endl;
        return VK_FALSE;
    }
};

int main() {
    Application app;

    try {
        app.run();
    }
    catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}